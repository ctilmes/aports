# Maintainer: Simon Zeni <simon@bl4ckb0ne.ca>
# Contributor: Fabricio Silva <hi@fabricio.dev>
pkgname=jellyfin
pkgver=10.9.1
pkgrel=0
pkgdesc="The Free Software Media System"
install="$pkgname.pre-install"
url="https://jellyfin.org/"
arch="x86_64 armv7 aarch64"
license="GPL-2.0-only"
makedepends="dotnet8-sdk"
depends="aspnetcore8-runtime ffmpeg"
subpackages="$pkgname-openrc"
source="$pkgname-$pkgver.tar.gz::https://github.com/jellyfin/jellyfin/archive/refs/tags/v$pkgver.tar.gz
	$pkgname.initd
	$pkgname.confd"

build() {
	dotnet publish Jellyfin.Server \
		--configuration Release \
		--no-self-contained \
		--use-current-runtime \
		--output publish
}

check() {
	dotnet test --no-restore
}

package() {
	mkdir -p "$pkgdir"/usr/lib "$pkgdir"/usr/bin

	cp -a publish "$pkgdir"/usr/lib/jellyfin
	ln -s ../lib/jellyfin/jellyfin "$pkgdir"/usr/bin/jellyfin

	install -Dm755 "$srcdir"/$pkgname.initd \
		"$pkgdir"/etc/init.d/$pkgname
	install -Dm644 "$srcdir"/$pkgname.confd \
		"$pkgdir"/etc/conf.d/$pkgname
}

sha512sums="
9527bdf7fca84087eeff9b70ef3884824758d54fcbf2e5635a1641c0b0660839ad27c632259947682acca38a50c8a6f89566d78d5e25355cccecb02544624590  jellyfin-10.9.1.tar.gz
bfd2f909fee25d1009ebd0b37d18ac0e9779a7310e5249b9ed4e1a4ef97ce620fe35df171a2e22d02171f88727838ab7b6a21822b80d86c957d87af6b717020b  jellyfin.initd
53bd2ab90cd2023765f763cf66cfd09ccc641508cad9339dedc07275afc8a4e39b8c992ec14c082b0396abdfdf63bab388567bb9c4e2bc5e477e714cc1ab9607  jellyfin.confd
"
